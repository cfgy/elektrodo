# Elektrodo

Behavior of common configuration of electrodes.


# Dependencies

* [SciPy: Scientific Python](http://www.scipy.org/docs.html): Numpy, Scipy, Mathplotlib, IPython, Sympy and Pandas.

* [FiPy: finite volume simulations](http://www.ctcms.nist.gov/fipy/) in Python.

* [mpmath: real and complex floating-point arithmetic with arbitrary precision](http://mpmath.org). In particular [jacobian elliptic functions](http://mpmath.org/doc/current/functions/elliptic.html).

